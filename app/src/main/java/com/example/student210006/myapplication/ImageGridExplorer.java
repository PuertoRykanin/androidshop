package com.example.student210006.myapplication;

import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.GridView;

        import android.support.v7.app.AppCompatActivity;
        import android.os.Bundle;
        import android.util.Log;
        import android.widget.Button;
        import android.app.Activity;
        import android.view.View;
        import android.view.View.OnClickListener;
        import android.database.Cursor;
        import android.database.sqlite.*;
        import android.content.*;
        import android.database.sqlite.SQLiteDatabase;
        import android.database.sqlite.SQLiteOpenHelper;
        import android.util.Log;
        import android.widget.GridView;

public class ImageGridExplorer extends AppCompatActivity {
    myDbAdapter DatabaseHandler; // Zmienna do zarządzania baza danych
    GridView gridView;
    static String[ ] GRID_DATA;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        Cursor cursor;
        SQLiteDatabase db;
        Context context;
        String value="";
        DatabaseHandler = new myDbAdapter(this);
        Log.d("what: ", "in onCreate start");
        super.onCreate(savedInstanceState);
        setContentView(R.layout.imagegridview);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        // Sets the Toolbar to act as the ActionBar for this Activity window.
        // Make sure the toolbar exists in the activity and is not null

        toolbar.setTitle("Lista Obrazów");
        setSupportActionBar(toolbar);
        //clickButton = (Button) findViewById(R.id.button2);
        cursor = null;
        db = DatabaseHandler.myhelper.getWritableDatabase();

        gridView = (GridView) findViewById(R.id.gridImageLayout);

        // Set custom adapter (GridAdapter) to gridview
        Bundle extras = getIntent().getExtras();
        if (extras != null) {
            value = extras.getString("itemID");
        }
        Log.e("values of data",value);
        GRID_DATA = new String[myDbAdapter.myhelper.getSpecifiedImageCount(Integer.valueOf(value))];
        GRID_DATA = myDbAdapter.myhelper.getSpecifiedImages(Integer.valueOf(value));

        gridView.setAdapter(  new ImageGridViewAdapter( this, GRID_DATA ) );

    }

    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_back, menu);

        return true;
    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.backArrow:
                Intent intent = new Intent(ImageGridExplorer.this, MainActivity.class);
                startActivity(intent);
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }
}


