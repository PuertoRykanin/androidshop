package com.example.student210006.myapplication;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.Drawable;
import android.os.Environment;
import android.support.v4.content.res.ResourcesCompat;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.GridView;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.BaseAdapter;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

public class ImageGridViewAdapter extends BaseAdapter {
    private Context context;
    private final String[] gridValues;
    static int idOfProductHandle = 0;


    //Constructor to initialize values
    public ImageGridViewAdapter(Context context, String[] gridValues) {

        this.context        = context;
        this.gridValues     = gridValues;
    }

    @Override
    public int getCount() {

        //return 25;
        // Number of times getView method call depends upon gridValues.length
        //NA SZTYWNO NA CHWILE TA 5
        return gridValues.length;
    }

    @Override
    public Object getItem(int position) {

        return null;
    }

    @Override
    public long getItemId(int position) {

        return 0;
    }


    // Number of times getView method call depends upon gridValues.length

    public View getView(int position, View convertView, ViewGroup parent) {

        // LayoutInflator to call external grid_item.xml file

        LayoutInflater inflater = (LayoutInflater) context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        View gridView;
        if (convertView == null) {

            gridView = new View(context);

            // get layout from grid_item.xml ( Defined Below )

            gridView = inflater.inflate( R.layout.gridviewlayoutimage , null);

            // set value into textview


            ImageView gameImageView =  (ImageView) gridView
                    .findViewById(R.id.imageFromGrid);


            if(idOfProductHandle < gridValues.length) {
                //int a = myDbAdapter.myhelper.getIdOfImage(gridValues[idOfProductHandle]);
                gameImageView.setImageResource(Integer.valueOf(gridValues[idOfProductHandle]));

            }







            idOfProductHandle++;

        } else {

            gridView = (View) convertView;
        }
        //HANDLER NA PUSTKE W ACTIVITY
        if(idOfProductHandle>gridValues.length){
            idOfProductHandle=0;
        }
        return gridView;
    }
}