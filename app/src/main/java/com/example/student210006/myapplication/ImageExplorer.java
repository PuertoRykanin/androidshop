package com.example.student210006.myapplication;

import android.support.v4.view.MotionEventCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.widget.Button;
import android.app.Activity;
import android.view.View;
import android.view.View.OnClickListener;
import android.database.Cursor;
import android.database.sqlite.*;
import android.content.*;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;
import android.widget.GridView;
import android.widget.ImageButton;
import android.widget.ImageView;

import com.example.student210006.myapplication.GridViewAdapter;
import com.example.student210006.myapplication.MainActivity;
import com.example.student210006.myapplication.R;
import com.example.student210006.myapplication.myDbAdapter;

import java.util.ArrayList;
import java.util.List;

import static com.example.student210006.myapplication.GridViewAdapter.idOfProductHandle;

public class ImageExplorer extends AppCompatActivity {
    myDbAdapter DatabaseHandler; // Zmienna do zarządzania baza danych
    int objCountGlob, placeOfCurrentImg, placeOfGroupImage;
    ImageView imageMain, imageSmall1, imageSmall2, imageSmall3;
    float startXValue = 1;

    @Override
    public boolean onTouchEvent(MotionEvent event) {
        float endXValue = 0;
        float x1 = event.getAxisValue(MotionEvent.AXIS_X);
        int action = MotionEventCompat.getActionMasked(event);
        switch (action) {
            case (MotionEvent.ACTION_DOWN):
                startXValue = event.getAxisValue(MotionEvent.AXIS_X);

                return true;

            case (MotionEvent.ACTION_UP):
                endXValue = event.getAxisValue(MotionEvent.AXIS_X);
                if (endXValue > startXValue) {
                    if (endXValue - startXValue > 100) {

                        if(placeOfCurrentImg-1>0){
                            placeOfCurrentImg--;
                            //i=placeOfCurrentImg;
                            imageMain.setImageResource(myDbAdapter.myhelper.getIdOfImage(placeOfCurrentImg));

                        }
                        else{
                            placeOfCurrentImg=objCountGlob;
                            //placeOfCurrentImg=i;
                            imageMain.setImageResource(myDbAdapter.myhelper.getIdOfImage(placeOfCurrentImg));
                        }
                    }
                }else {
                    if (startXValue -endXValue> 100) {
                        if(placeOfCurrentImg<objCountGlob){
                            placeOfCurrentImg++;
                            //i++;
                            imageMain.setImageResource(myDbAdapter.myhelper.getIdOfImage(placeOfCurrentImg));

                        }
                        else{
                            //i=1;
                            placeOfCurrentImg=1;
                            imageMain.setImageResource(myDbAdapter.myhelper.getIdOfImage(placeOfCurrentImg));
                        }
                    }
                }
                return true;


            default:
                return super.onTouchEvent(event);
        }

    }
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        Cursor cursor;
        SQLiteDatabase db;
        Context context;
        DatabaseHandler = new myDbAdapter(this);
        Log.d("what: ", "in onCreate start");
        super.onCreate(savedInstanceState);
        setContentView(R.layout.imageexplorer);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        // Sets the Toolbar to act as the ActionBar for this Activity window.
        // Make sure the toolbar exists in the activity and is not null

        toolbar.setTitle("Przeglądanie Obrazów");
        setSupportActionBar(toolbar);
        //clickButton = (Button) findViewById(R.id.button2);

        ImageButton imageLeft, imageRight, imageSmallLeft, imageSmallRight;
        cursor = null;
        db = DatabaseHandler.myhelper.getWritableDatabase();
        objCountGlob = myDbAdapter.myhelper.getAllProductCount();
        placeOfCurrentImg = 1;
        placeOfGroupImage = 1;
        imageMain = (ImageView) findViewById(R.id.imageMain);
        imageSmall1 = (ImageView) findViewById(R.id.imageSmall1);
        imageSmall2 = (ImageView) findViewById(R.id.imageSmall2);
        imageSmall3 = (ImageView) findViewById(R.id.imageSmall3);
        imageLeft = (ImageButton) findViewById(R.id.imageLeftSwitch);
        imageRight = (ImageButton) findViewById(R.id.imageRightSwitch);
        imageSmallLeft = (ImageButton) findViewById(R.id.imageGroupLeft);
        imageSmallRight = (ImageButton) findViewById(R.id.imageGroupRight);

        imageMain.setImageResource(myDbAdapter.myhelper.getIdOfImage(1));
        imageSmall1.setImageResource(myDbAdapter.myhelper.getIdOfImage(1));
        imageSmall2.setImageResource(myDbAdapter.myhelper.getIdOfImage(2));
        imageSmall3.setImageResource(myDbAdapter.myhelper.getIdOfImage(3));

        imageRight.setOnClickListener(new View.OnClickListener() {
            //int i= placeOfCurrentImg;
            int objCount = objCountGlob;
            @Override
            public void onClick(View v) {
                if(placeOfCurrentImg<objCount){
                    placeOfCurrentImg++;
                    //i++;
                    imageMain.setImageResource(myDbAdapter.myhelper.getIdOfImage(placeOfCurrentImg));

                }
                else{
                    //i=1;
                    placeOfCurrentImg=1;
                    imageMain.setImageResource(myDbAdapter.myhelper.getIdOfImage(placeOfCurrentImg));
                }
            }
        });
        imageLeft.setOnClickListener(new View.OnClickListener() {
            //int i= placeOfCurrentImg;
            int objCount = objCountGlob;
            @Override
            public void onClick(View v) {
                if(placeOfCurrentImg-1>0){
                    placeOfCurrentImg--;
                    //i=placeOfCurrentImg;
                    imageMain.setImageResource(myDbAdapter.myhelper.getIdOfImage(placeOfCurrentImg));

                }
                else{
                    placeOfCurrentImg=objCountGlob;
                    //placeOfCurrentImg=i;
                    imageMain.setImageResource(myDbAdapter.myhelper.getIdOfImage(placeOfCurrentImg));
                }
            }
        });
        imageSmallRight.setOnClickListener(new View.OnClickListener() {
            //int i= placeOfCurrentImg;
            int objCount = objCountGlob;
            @Override
            public void onClick(View v) {
                if(placeOfGroupImage+3<=objCount){
                    placeOfGroupImage+=1;
                    imageSmall1.setImageResource(myDbAdapter.myhelper.getIdOfImage(placeOfGroupImage));
                    imageSmall2.setImageResource(myDbAdapter.myhelper.getIdOfImage(placeOfGroupImage+1));
                    imageSmall3.setImageResource(myDbAdapter.myhelper.getIdOfImage(placeOfGroupImage+2));

                }
                else{
                    placeOfGroupImage=1;
                    imageSmall1.setImageResource(myDbAdapter.myhelper.getIdOfImage(placeOfGroupImage));
                    imageSmall2.setImageResource(myDbAdapter.myhelper.getIdOfImage(placeOfGroupImage+1));
                    imageSmall3.setImageResource(myDbAdapter.myhelper.getIdOfImage(placeOfGroupImage+2));
                }
            }
        });
        imageSmallLeft.setOnClickListener(new View.OnClickListener() {
            //int i= placeOfCurrentImg;
            int objCount = objCountGlob;
            @Override
            public void onClick(View v) {
                if(placeOfGroupImage-1>0){
                    placeOfGroupImage-=1;
                    imageSmall1.setImageResource(myDbAdapter.myhelper.getIdOfImage(placeOfGroupImage));
                    imageSmall2.setImageResource(myDbAdapter.myhelper.getIdOfImage(placeOfGroupImage+1));
                    imageSmall3.setImageResource(myDbAdapter.myhelper.getIdOfImage(placeOfGroupImage+2));

                }
                else{
                    placeOfGroupImage=objCount-2;
                    imageSmall1.setImageResource(myDbAdapter.myhelper.getIdOfImage(placeOfGroupImage));
                    imageSmall2.setImageResource(myDbAdapter.myhelper.getIdOfImage(placeOfGroupImage+1));
                    imageSmall3.setImageResource(myDbAdapter.myhelper.getIdOfImage(placeOfGroupImage+2));
                }
            }
        });
        imageSmall1.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                List<ProductDTO> temp = new ArrayList<ProductDTO>();
                if(placeOfGroupImage == objCountGlob){
                    temp = myDbAdapter.myhelper.searchProducts(myDbAdapter.myhelper.selectSearchProducts(placeOfGroupImage-2,"-","-",0,"-","-","-"));

                }
                else{
                    temp = myDbAdapter.myhelper.searchProducts(myDbAdapter.myhelper.selectSearchProducts(placeOfGroupImage,"-","-",0,"-","-","-"));

                }
                placeOfCurrentImg= temp.get(0).getId();
                imageMain.setImageResource(myDbAdapter.myhelper.getIdOfImage(placeOfCurrentImg));


            }
        });
        imageSmall2.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                List<ProductDTO> temp = new ArrayList<ProductDTO>();
                temp = myDbAdapter.myhelper.searchProducts(myDbAdapter.myhelper.selectSearchProducts(placeOfGroupImage+1,"-","-",0,"-","-","-"));
                placeOfCurrentImg= temp.get(0).getId();
                imageMain.setImageResource(myDbAdapter.myhelper.getIdOfImage(placeOfCurrentImg));


            }
        });
        imageSmall3.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                List<ProductDTO> temp = new ArrayList<ProductDTO>();
                temp = myDbAdapter.myhelper.searchProducts(myDbAdapter.myhelper.selectSearchProducts(placeOfGroupImage+2,"-","-",0,"-","-","-"));
                placeOfCurrentImg= temp.get(0).getId();
                imageMain.setImageResource(myDbAdapter.myhelper.getIdOfImage(placeOfCurrentImg));


            }
        });

    }

    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_back, menu);

        return true;
    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.backArrow:
                Intent intent = new Intent(ImageExplorer.this, MainActivity.class);
                startActivity(intent);
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }
}