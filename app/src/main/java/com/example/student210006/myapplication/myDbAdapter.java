package com.example.student210006.myapplication;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.graphics.Bitmap;
import android.util.Log;

import java.io.ByteArrayOutputStream;
import java.lang.String;
import java.sql.Blob;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class myDbAdapter {
    static myDbHelper myhelper;
    public myDbAdapter(Context context)
    {
        myhelper = new myDbHelper(context);
    }

    static class myDbHelper extends SQLiteOpenHelper
    {
        SQLiteDatabase db;
        private static final String DATABASE_NAME = "myDatabase";    // Database Name
        private static final int DATABASE_Version = 1;    // Database Version

        private static final String TABLE_NAME = "productTable";   // Table Name
        private static final String UID="id";     // Column I (Primary Key)
        private static final String NAME = "name";    //Column II
        private static final String CATEGORY= "category";    // Column III
        private static final String PRICE = "price";
        private static final String PLAYERS = "players";
        private static final String TYPE = "type";
        private static final String IMAGE = "image";

        private static final String TABLE_NAME2 = "searchTable";
        private static final String SEARCHES="search";

        private static final String TABLE_NAME3 = "favouritesTable";
        private static final String FAVOURITES="favourites";

        private static final String TABLE_NAME4 = "basketTable";
        private static final String BASKET="basket";

        private static final String TABLE_NAME5 = "screenTable";
        private static final String SCREEN="screen";
        private static final String ITEMID ="itemId";

        private static final String CREATE_TABLE = "CREATE TABLE "+TABLE_NAME+
                " ("+UID+" INTEGER PRIMARY KEY AUTOINCREMENT, "+NAME+" VARCHAR(255) ,"+ CATEGORY+" VARCHAR(225) ,"
                +PRICE+" DECIMAL(7,2) ,"+PLAYERS+" VARCHAR(255) ,"+TYPE+" VARCHAR(255) ,"+ IMAGE+" VARCHAR(225));";
        private static final String DROP_TABLE ="DROP TABLE IF EXISTS "+TABLE_NAME;

        private static final String CREATE_TABLE2 = "CREATE TABLE "+TABLE_NAME2+
                " ("+UID+" INTEGER PRIMARY KEY AUTOINCREMENT, "+SEARCHES+" VARCHAR(255));";
        private static final String DROP_TABLE2 ="DROP TABLE IF EXISTS "+TABLE_NAME2;

        private static final String CREATE_TABLE3 = "CREATE TABLE "+TABLE_NAME3+
                " ("+UID+" INTEGER PRIMARY KEY AUTOINCREMENT, "+FAVOURITES+" VARCHAR(255));";
        private static final String DROP_TABLE3 ="DROP TABLE IF EXISTS "+TABLE_NAME3;

        private static final String CREATE_TABLE4 = "CREATE TABLE "+TABLE_NAME4+
                " ("+UID+" INTEGER PRIMARY KEY AUTOINCREMENT, "+BASKET+" VARCHAR(255));";
        private static final String DROP_TABLE4 ="DROP TABLE IF EXISTS "+TABLE_NAME4;

        private static final String CREATE_TABLE5 = "CREATE TABLE "+TABLE_NAME5+
                " ("+UID+" INTEGER PRIMARY KEY AUTOINCREMENT, "+ITEMID+" INTEGER ,"+ SCREEN+" VARCHAR(225));";
        private static final String DROP_TABLE5 ="DROP TABLE IF EXISTS "+TABLE_NAME5;

        private Context context;

        public myDbHelper(Context context) {
            super(context, DATABASE_NAME, null, DATABASE_Version);
            this.context=context;
        }

        public void onCreate(SQLiteDatabase dbb) {
            db =dbb;
            try {
                db.execSQL(CREATE_TABLE);
                db.execSQL(CREATE_TABLE2);
                db.execSQL(CREATE_TABLE3);
                db.execSQL(CREATE_TABLE4);
                db.execSQL(CREATE_TABLE5);



            } catch (Exception e) {
                Log.d("xxx","DATABASECREATED");
            }
            //db.close();
            fillDatabase();
        }

        public void fillDatabase(){ //DO UZUPEŁNIENIA BAZA DANYCH ZAWARTOŚĆ

            addToProducts("Twierdza", "7+", "110", "1-4", "CCG",String.valueOf(R.drawable.game1) );
            addToProducts("Blink", "72+", "110", "1-4", "FIGHTING",String.valueOf(R.drawable.game2) );
            addToProducts("Grandpa", "712+", "300", "1-7", "TCG",String.valueOf(R.drawable.game3) );
            addToProducts("Starcraft Board Game", "7+", "88", "1-9", "CCG",String.valueOf(R.drawable.game4) );
            addToProducts("Warhammer", "2+", "1100", "1-4", "FIGHTING",String.valueOf(R.drawable.game5) );
            addToProducts("Warhammer Podbój", "2+", "77", "1-2", "TCG",String.valueOf(R.drawable.game6) );

            addToScreens(1,String.valueOf(R.drawable.game1));
            addToScreens(1,String.valueOf(R.drawable.game1s1));
            addToScreens(1,String.valueOf(R.drawable.game1s2));
            addToScreens(2,String.valueOf(R.drawable.game2));
            addToScreens(2,String.valueOf(R.drawable.game2s1));
            addToScreens(3,String.valueOf(R.drawable.game3));
            addToScreens(4,String.valueOf(R.drawable.game4));
            addToScreens(5,String.valueOf(R.drawable.game5));
            addToScreens(6,String.valueOf(R.drawable.game6));


        }
        public void deleteDataBase()
        {
            db = myhelper.getWritableDatabase();
            db.execSQL(DROP_TABLE);
            db.execSQL(DROP_TABLE2);
            db.execSQL(DROP_TABLE3);
            db.execSQL(DROP_TABLE4);
            db.execSQL(DROP_TABLE5);
            //db.close();
        }
        public int getAllProductCount(){
        db = myhelper.getWritableDatabase();
        Cursor c = db.rawQuery("SELECT * FROM productTable;",null);
        //db.close();
        return c.getCount();
    }
        public int[] getAllProductIds(){
            db = myhelper.getWritableDatabase();
            Cursor c = db.rawQuery("SELECT * FROM productTable;",null);
            int[] temp = new int[c.getCount()];
            int i = 0;
            if (c.moveToFirst()) {
                while ( !c.isAfterLast() ) {
                    temp[i]= c.getInt(0);
                    i++;
                    c.moveToNext();
                }
            }
            //db.close();
            return temp;
        }
        public int getSpecifiedImageCount(int id){
            db = myhelper.getWritableDatabase();
            Cursor c = db.rawQuery("SELECT * FROM screenTable WHERE itemId='"+id+"'",null);
            int temp = c.getCount();
            //db.close();
            return temp;
        }
        public String[] getSpecifiedImages(int id){
            db = myhelper.getWritableDatabase();
            Cursor c = db.rawQuery("SELECT * FROM screenTable WHERE itemId='"+id+"'",null);
            String[] temp = new String[c.getCount()];
            int i = 0;
            if (c.moveToFirst()) {
                while ( !c.isAfterLast() ) {
                    temp[i]= c.getString(2);
                    i++;
                    c.moveToNext();
                }
            }
            //db.close();
            return temp;
        }
        public int getAllFavouritesCount(){
            db = myhelper.getWritableDatabase();
            db = myhelper.getWritableDatabase();
            Cursor c = db.rawQuery("SELECT * FROM favouritesTable;",null);
            //db.close();
            return c.getCount();
        }
        public int[] getAllFavouritesIds(){
            db = myhelper.getWritableDatabase();
            db = myhelper.getWritableDatabase();

            Cursor c = db.rawQuery("SELECT * FROM favouritesTable;",null);
            int[] temp = new int[c.getCount()];
            int i = 0;
            if (c.moveToFirst()) {
                while ( !c.isAfterLast() ) {
                    temp[i]= c.getInt(1);
                    i++;
                    c.moveToNext();
                }
            }
            //db.close();
            return temp;
        }

        public int getAllBasketCount(){
            //myhelper.db.
            db = myhelper.getWritableDatabase();
            db = myhelper.getWritableDatabase();
            Cursor c = db.rawQuery("SELECT * FROM basketTable;",null);
            int temp = c.getCount();
            //db.close();
            return temp;
        }
        public int[] getAllBasketIds(){
            //myhelper = new myDbHelper(context);
            db = myhelper.getWritableDatabase();
            Cursor c = db.rawQuery("SELECT * FROM basketTable;",null);
            int[] temp = new int[c.getCount()];
            int i = 0;
            if (c.moveToFirst()) {
                while ( !c.isAfterLast() ) {
                    temp[i]= c.getInt(1);
                    i++;
                    c.moveToNext();
                }
            }
            //db.close();
            return temp;
        }


        public void addToProducts(String n, String c, String pr, String pl, String t, String i){
            //db = myhelper.getWritableDatabase();
            ContentValues contentValues = new ContentValues();

            contentValues.put(myDbHelper.NAME, n);
            contentValues.put(myDbHelper.CATEGORY, c);
            contentValues.put(myDbHelper.PRICE, pr);
            contentValues.put(myDbHelper.PLAYERS, pl);
            contentValues.put(myDbHelper.TYPE, t);
            contentValues.put(myDbHelper.IMAGE, i);
            db.insert(myDbHelper.TABLE_NAME, null , contentValues);
            //db.close();
        }
        public void addToScreens(int itemId, String imgPath){

            db = myhelper.getWritableDatabase();
            ContentValues contentValues = new ContentValues();

            contentValues.put(myDbHelper.SCREEN, imgPath);
            contentValues.put(myDbHelper.ITEMID, itemId);
            db.insert(myDbHelper.TABLE_NAME5, null , contentValues);
            //db.close();
        }
        public void addToSearch(String inputSearch){

            db = myhelper.getWritableDatabase();
            ContentValues contentValues = new ContentValues();

            contentValues.put(myDbHelper.SEARCHES, inputSearch);
            db.insert(myDbHelper.TABLE_NAME2, null , contentValues);
            //db.close();
        }
        public boolean addOrRemoveToFavourites(int id){
            db = myhelper.getWritableDatabase();
            ContentValues contentValues = new ContentValues();

            if(searchIfInFavourites(id)) {
                removeFav(id);
                return false;
            }
            contentValues.put(myDbHelper.FAVOURITES, id);
            db.insert(myDbHelper.TABLE_NAME3,null,contentValues);
            //db.close();
            return true;
        }
        public boolean addOrRemoveToBasket(int id){
            db = myhelper.getWritableDatabase();
            ContentValues contentValues = new ContentValues();

            if(searchIfInBasket(id)) {
                removeBasket(id);
                return false;
            }
            contentValues.put(myDbHelper.BASKET, id);
            db.insert(myDbHelper.TABLE_NAME4,null,contentValues);
            //db.close();
            return true;
        }
        public boolean searchIfInBasket(int id){
            db = myhelper.getWritableDatabase();
            Cursor c = db.rawQuery("SELECT * FROM basketTable WHERE basket="+id+";", null);
            //db.close();
            if(c.getCount()>=1){
                return true;
            }
            return false;
        }
        public boolean searchIfInFavourites(int id){

            db = myhelper.getWritableDatabase();
            Cursor c = db.rawQuery("SELECT * FROM favouritesTable WHERE favourites="+id+";", null);
            //db.close();
            if(c.getCount()>=1){
                return true;
            }
            return false;
        }
        public void removeFav(int id){
            db = myhelper.getWritableDatabase();
            db.execSQL("DELETE FROM favouritesTable WHERE favourites="+id+";");
            //db.close();
        }
        public void removeBasket(int id){
            db = myhelper.getWritableDatabase();
            db.execSQL("DELETE FROM basketTable WHERE basket="+id+";");
            //db.close();
        }
        public int getIdOfImage(int id){
            String search;

            search = "SELECT * FROM productTable WHERE id="+id+";";
            return Integer.valueOf(searchProducts(search).get(0).getImage());
        }
        public String selectSearchProducts(int id, String n, String c, double pr, String pl, String t, String i){
            String search;

            search = "SELECT * FROM productTable WHERE";

            if(id != 0){
                search+= " id='"+id+"' AND";
            }
            if(n != "-"){
                search+= " name='"+n+"' AND";
            }
            if(c != "-"){
                search+= " category='"+c+"' AND";
            }
            if(pr != 0){
                search+= " price='"+pr+"' AND";
            }
            if(pl != "-"){
                search+= " players='"+pl+"' AND";
            }
            if(t != "-"){
                search+= " type='"+t+"' AND";
            }
            if(i != "-"){
                search+= " image='"+i+"' AND";
            }
            search = search.substring(0, search.length()-3);
            search+= ";";
            return search;
        }
        public String advancedSearchProducts(String n, String c, String pr, String pl, String t){
            String search;

            search = "SELECT * FROM productTable WHERE";


            if(!n.trim().equals("-") && !n.trim().equals("")){
                search+= " name LIKE '"+n+"' AND";
            }
            if(c != "-"){
                search+= " category='"+c+"' AND";
            }
            if(pr != "-"){
                Pattern regex = Pattern.compile("(\\d+(?:\\.\\d+)?)");
                Matcher matcher = regex.matcher(pr);
                double[] temp = new double[2];
                int i=0;
                while(matcher.find()){
                    temp[i] = Double.valueOf(matcher.group(1));
                    i++;
                }
                search+= " price>="+temp[0]+" AND price<="+temp[1]+ " AND";
            }
            if(pl != "-"){
                search+= " players='"+pl+"' AND";
            }
            if(t != "-"){
                search+= " type='"+t+"' AND";
            }

            search = search.substring(0, search.length()-3);
            search+= ";";
            Log.e("search construct", search);
            return search;
        }
        public List<ProductDTO> searchProducts(String selectString){

            db = myhelper.getWritableDatabase();
            ContentValues contentValues = new ContentValues();
            int i = 0;
            List<ProductDTO> dtoList = new ArrayList<ProductDTO>();
            Cursor c = db.rawQuery(selectString, null);

            if (c.moveToFirst()) {
                while ( !c.isAfterLast() ) {

                    ProductDTO tempdto= new ProductDTO();
                    tempdto.setId(c.getInt(0));
                    tempdto.setName(c.getString(1));
                    tempdto.setCategory(c.getString(2));
                    tempdto.setPrice(c.getDouble(3));
                    tempdto.setPlayers(c.getString(4));
                    tempdto.setType(c.getString(5));
                    tempdto.setImage(c.getString(6));

                    dtoList.add(tempdto);
                    c.moveToNext();
                }
            }
            //db.close();
            return dtoList;
        }

        public int[] searchProductsIds(String selectString){

            db = myhelper.getWritableDatabase();
            ContentValues contentValues = new ContentValues();
            int i = 0;

            Cursor c = db.rawQuery(selectString, null);
            int[] tempArray = new int[c.getCount()];
            if (c.moveToFirst()) {
                while ( !c.isAfterLast() ) {

                    tempArray[i] = c.getInt(0);
                    i++;

                    c.moveToNext();
                }
            }
            //db.close();
            return tempArray;
        }
        public static byte[] getBitmapAsByteArray(Bitmap bitmap) {
            ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
            bitmap.compress(Bitmap.CompressFormat.PNG, 0, outputStream);
            return outputStream.toByteArray();
        }

        @Override
        public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
            try {
                db.execSQL(DROP_TABLE);
                onCreate(db);
            }catch (Exception e) {

            }
        }
    }
}