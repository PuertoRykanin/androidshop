package com.example.student210006.myapplication;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.view.View;
import android.database.Cursor;
import android.content.*;
import android.database.sqlite.SQLiteDatabase;
import android.widget.EditText;
import android.widget.GridView;
import android.widget.Spinner;

import java.util.ArrayList;
import java.util.List;

public class SearchActivity extends AppCompatActivity {
    myDbAdapter DatabaseHandler; // Zmienna do zarządzania baza danych
    GridView gridView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        //Button clickButton;
        Cursor cursor;
        SQLiteDatabase db;
        Context context;
        DatabaseHandler = new myDbAdapter(this);
        Log.d("what: ", "in onCreate start");
        super.onCreate(savedInstanceState);
        setContentView(R.layout.searchview);


        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        // Sets the Toolbar to act as the ActionBar for this Activity window.
        // Make sure the toolbar exists in the activity and is not null

        toolbar.setTitle("Wyszukiwanie");
        setSupportActionBar(toolbar);
        //clickButton = (Button) findViewById(R.id.ThingButton);

        db = DatabaseHandler.myhelper.getWritableDatabase();

        String empName = "";

        Spinner priceSpinner = (Spinner) findViewById(R.id.spinnerPrice);
        Spinner categorySpinner = (Spinner) findViewById(R.id.spinnerCategory);
        Spinner typeSpinner = (Spinner) findViewById(R.id.spinnerType);
        Spinner playersSpinner = (Spinner) findViewById(R.id.spinnerPlayers);

        List<String> spinnerArray1 = new ArrayList<String>();

        spinnerArray1.add("-");
        spinnerArray1.add("1-3");
        spinnerArray1.add("4-13");
        spinnerArray1.add("14-17");
        spinnerArray1.add("18+");

        ArrayAdapter<String> adapter1 = new ArrayAdapter<String>(this,
                android.R.layout.simple_spinner_dropdown_item, spinnerArray1);

        categorySpinner.setAdapter(adapter1);

        List<String> spinnerArray2 = new ArrayList<String>();

        spinnerArray2.add("-");
        spinnerArray2.add("CCG");
        spinnerArray2.add("TCG");
        spinnerArray2.add("Fighting");
        spinnerArray2.add("RPG");

        ArrayAdapter<String> adapter2 = new ArrayAdapter<String>(this,
                android.R.layout.simple_spinner_dropdown_item, spinnerArray2);

        typeSpinner.setAdapter(adapter2);

        List<String> spinnerArray3 = new ArrayList<String>();

        spinnerArray3.add("-");
        spinnerArray3.add("1-4");
        spinnerArray3.add("1-2");
        spinnerArray3.add("7+");
        spinnerArray3.add("1");

        ArrayAdapter<String> adapter3 = new ArrayAdapter<String>(this,
                android.R.layout.simple_spinner_dropdown_item, spinnerArray3);

        playersSpinner.setAdapter(adapter3);

        List<String> spinnerArray4 = new ArrayList<String>();

        spinnerArray4.add("-");
        spinnerArray4.add("0-50");
        spinnerArray4.add("50.01-100");
        spinnerArray4.add("100.01-200");
        spinnerArray4.add("200.01-2000");

        ArrayAdapter<String> adapter4 = new ArrayAdapter<String>(this,
                android.R.layout.simple_spinner_dropdown_item, spinnerArray4);

        priceSpinner.setAdapter(adapter4);
        //db.execSQL("DROP TABLE IF EXISTS myTable");

        //db.rawQuery("DROP TABLE IF EXISTS myTable", null);
        /*Cursor c = db.rawQuery("SELECT name FROM sqlite_master WHERE type='table'", null);


        if (c.moveToFirst()) {
            while ( !c.isAfterLast() ) {
                //Toast.makeText(activityName.this, "Table Name=> "+c.getString(0), Toast.LENGTH_LONG).show();
                Log.e("xxx", c.getString(0));
                c.moveToNext();
            }
        }


            cursor = db.rawQuery("SELECT name FROM productTable",null);
            List<ProductDTO> tempdto = new ArrayList<ProductDTO>();
            tempdto = myDbAdapter.myhelper.searchProducts(myDbAdapter.myhelper.selectSearchProducts(0,"-","-",110,"-","-","-"));
            Log.e("tagSelect",tempdto.get(0).getName());
            Log.e("tagSelect",tempdto.get(1).getName());
            //if(cursor.getCount() > 0) {

        if (cursor.moveToFirst()) {
            while ( !cursor.isAfterLast() ) {
                //Toast.makeText(activityName.this, "Table Name=> "+c.getString(0), Toast.LENGTH_LONG).show();
                empName = cursor.getString(cursor.getColumnIndex("name"));
                Log.e("xxx",empName );
                cursor.moveToNext();
            }
        }

            //}


            cursor.close();
        */
        //clickButton.setText("elblow");



    }
    public void goFavourites(View view){
        Intent intent = new Intent(SearchActivity.this, FavouritesExplorer.class);
        startActivity(intent);
    }
    public void goBasket(View view){
        Intent intent = new Intent(SearchActivity.this, BasketExplorer.class);
        startActivity(intent);
    }
    public void addSearch(View view){
        EditText input = (EditText) findViewById(R.id.inputText);
        DatabaseHandler = new myDbAdapter(this);
        myDbAdapter.myhelper.addToSearch(input.getText().toString());
    }
    public void goProducts(View view){
        Intent intent = new Intent(SearchActivity.this, ProductExplorer.class);
        startActivity(intent);
    }
    public void search(View view){
        EditText input = (EditText) findViewById(R.id.inputText);
        Spinner priceSpinner = (Spinner) findViewById(R.id.spinnerPrice);
        Spinner categorySpinner = (Spinner) findViewById(R.id.spinnerCategory);
        Spinner typeSpinner = (Spinner) findViewById(R.id.spinnerType);
        Spinner playersSpinner = (Spinner) findViewById(R.id.spinnerPlayers);

        String in,pr,cat,type,pla;
        Log.e("ifempty",input.getText().toString());
        in = input.getText().toString();

        cat = categorySpinner.getSelectedItem().toString();
        type = typeSpinner.getSelectedItem().toString();
        pla = playersSpinner.getSelectedItem().toString();
        pr = priceSpinner.getSelectedItem().toString();
        int[] temp = myDbAdapter.myhelper.searchProductsIds(myDbAdapter.myhelper.advancedSearchProducts(in,cat,pr,pla,type));
        Log.e("asd", String.valueOf(temp.length));
        gridView = (GridView) findViewById(R.id.searchGrid);
        gridView.setAdapter(  new GridViewAdapter( this, temp ) );
    }
}