package com.example.student210006.myapplication;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Button;
import android.app.Activity;
import android.view.View;
import android.view.View.OnClickListener;
import android.database.Cursor;
import android.database.sqlite.*;
import android.content.*;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;
import android.widget.GridView;

import com.example.student210006.myapplication.GridViewAdapter;
import com.example.student210006.myapplication.MainActivity;
import com.example.student210006.myapplication.R;
import com.example.student210006.myapplication.myDbAdapter;

public class BasketExplorer extends AppCompatActivity {
    myDbAdapter DatabaseHandler; // Zmienna do zarządzania baza danych
    GridView gridView;
    static int[ ] GRID_DATA;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        Cursor cursor;
        SQLiteDatabase db;
        Context context;
        DatabaseHandler = new myDbAdapter(this);
        Log.d("what: ", "in onCreate start");
        super.onCreate(savedInstanceState);
        setContentView(R.layout.basketexplorerview);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        // Sets the Toolbar to act as the ActionBar for this Activity window.
        // Make sure the toolbar exists in the activity and is not null

        toolbar.setTitle("Koszyk");
        setSupportActionBar(toolbar);
        //clickButton = (Button) findViewById(R.id.button2);
        cursor = null;
        //db = DatabaseHandler.myhelper.getWritableDatabase();

        gridView = (GridView) findViewById(R.id.gridView1);

        // Set custom adapter (GridAdapter) to gridview
        GRID_DATA = new int[myDbAdapter.myhelper.getAllBasketCount()];
        GRID_DATA = myDbAdapter.myhelper.getAllBasketIds();
        gridView.setAdapter(  new GridViewAdapter( this, GRID_DATA ) );

        String empName = "";

    }

    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_back, menu);

        return true;
    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.backArrow:
                Intent intent = new Intent(BasketExplorer.this, MainActivity.class);
                startActivity(intent);
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    public void goFavourites(View view){
        Intent intent = new Intent(BasketExplorer.this, FavouritesExplorer.class);
        startActivity(intent);
    }
    public void goProducts(View view){
        Intent intent = new Intent(BasketExplorer.this, ProductExplorer.class);
        startActivity(intent);
    }
    public void goSearch(View view){
        Intent intent = new Intent(BasketExplorer.this, SearchActivity.class);
        startActivity(intent);
    }
}