package com.example.student210006.myapplication;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.Drawable;
import android.os.Environment;
import android.support.v4.content.res.ResourcesCompat;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.GridView;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.BaseAdapter;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

public class GridViewAdapter extends BaseAdapter {
    private Context context;
    private final int[] gridValues;
    static int idOfProductHandle = 0;


    //Constructor to initialize values
    public GridViewAdapter(Context context, int[ ] gridValues) {

        this.context        = context;
        this.gridValues     = gridValues;
    }

    @Override
    public int getCount() {

        //return 25;
        // Number of times getView method call depends upon gridValues.length
        //NA SZTYWNO NA CHWILE TA 5
        return gridValues.length;
    }

    @Override
    public Object getItem(int position) {

        return null;
    }

    @Override
    public long getItemId(int position) {

        return 0;
    }


    // Number of times getView method call depends upon gridValues.length

    public View getView(int position, View convertView, ViewGroup parent) {

        // LayoutInflator to call external grid_item.xml file

        LayoutInflater inflater = (LayoutInflater) context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        View gridView;
        if (convertView == null) {

            gridView = new View(context);

            // get layout from grid_item.xml ( Defined Below )

            gridView = inflater.inflate( R.layout.gridviewlayout , null);

            // set value into textview

            TextView nameView = (TextView) gridView
                    .findViewById(R.id.textName);
            TextView priceView = (TextView) gridView
                    .findViewById(R.id.textPrice);
            final ImageButton basketButtonView = (ImageButton) gridView
                    .findViewById(R.id.basketButton);
            final ImageButton favouriteButtonView = (ImageButton) gridView
                    .findViewById(R.id.favouriteButton);
            ImageView gameImageView = (ImageView) gridView
                    .findViewById(R.id.gameImage);


            // Init values of gridview elements


            //File mFile1 = Environment.getExternalStorageDirectory();

            //ResourcesCompat.getDrawable(getResources(), R.drawable.name, null);
            //String fileName ="game1.png";
            //String sdPath = mFile1.getAbsolutePath().toString()+"/"+fileName;
            //File temp=new File(sdPath);
            //String.valueOf(R.drawable.game1);
            //BitmapFactory.Options options = new BitmapFactory.Options();
            //options.inPreferredConfig = Bitmap.Config.ARGB_8888;
            //Bitmap bitmap = BitmapFactory.decodeFile(, options);\
            Log.e("length?",String.valueOf(idOfProductHandle));
            if(idOfProductHandle < gridValues.length) {
                //int a = myDbAdapter.myhelper.getIdOfImage(gridValues[idOfProductHandle]);
                List<ProductDTO> temp = new ArrayList<ProductDTO>();
                temp = myDbAdapter.myhelper.searchProducts(myDbAdapter.myhelper.selectSearchProducts(gridValues[idOfProductHandle],"-","-",0,"-","-","-"));
                gameImageView.setImageResource(Integer.valueOf(temp.get(0).getImage()));
                nameView.setText(temp.get(0).getName());
                priceView.setText(String.valueOf(temp.get(0).getPrice()) + " zł");
                Log.e("item?",String.valueOf(gridValues[idOfProductHandle]));
            }

            if(idOfProductHandle < gridValues.length){
                //Button Basket initialization for gridView
                if(myDbAdapter.myhelper.searchIfInBasket(gridValues[idOfProductHandle])){
                    basketButtonView.setImageResource(R.drawable.basket1);
                }
                else {
                    basketButtonView.setImageResource(R.drawable.basket);
                }
                if(myDbAdapter.myhelper.searchIfInFavourites(gridValues[idOfProductHandle])){
                    favouriteButtonView.setImageResource(R.drawable.fav1);
                }
                else {
                    favouriteButtonView.setImageResource(R.drawable.fav);
                }

                basketButtonView.setOnClickListener(new View.OnClickListener() {
                    int i= gridValues[idOfProductHandle];
                    @Override
                    public void onClick(View v) {
                        Log.e("inBasketButton",myDbAdapter.myhelper.searchProducts(myDbAdapter.myhelper.selectSearchProducts(i,"-","-",0,"-","-","-")).get(0).getName());
                        if(!myDbAdapter.myhelper.addOrRemoveToBasket(i)){
                            //change look of fav icon
                            Log.e("FavData","removed from Basket");
                            basketButtonView.setImageResource(R.drawable.basket);
                        }
                        else {
                            Log.e("FavData", "added to Basket");
                            basketButtonView.setImageResource(R.drawable.basket1);
                        }
                    }
                });
                favouriteButtonView.setOnClickListener(new View.OnClickListener() {
                    int i= gridValues[idOfProductHandle];

                    @Override
                    public void onClick(View v) {
                        //Log.e("inBasketButton",myDbAdapter.myhelper.searchProducts(myDbAdapter.myhelper.selectSearchProducts(i,"-","-",0,"-","-","-")).get(0).getName());
                        if(!myDbAdapter.myhelper.addOrRemoveToFavourites(i)){
                            //change look of fav icon
                            Log.e("FavData","removed from Fav");
                            favouriteButtonView.setImageResource(R.drawable.fav);
                        }
                        else {
                            Log.e("FavData", "added to Fav");
                            favouriteButtonView.setImageResource(R.drawable.fav1);
                        }
                    }
                });

                gameImageView.setOnClickListener(new View.OnClickListener() {
                    int i= gridValues[idOfProductHandle];
                    @Override
                    public void onClick(View v) {

                        Intent intent = new Intent(context,DetailedExplorer.class);
                        intent.putExtra("itemID", i);
                        context.startActivity(intent);
                    }
                });
            }





            // set image based on selected text

            //ImageView imageView = (ImageView) gridView
                    //.findViewById(R.id.grid_item_image);

            //String arrLabel = gridValues[ position ];
            /*
            if (arrLabel.equals("Windows")) {

                imageView.setImageResource(R.drawable.windows_logo);

            } else if (arrLabel.equals("iOS")) {

                imageView.setImageResource(R.drawable.ios_logo);

            } else if (arrLabel.equals("Blackberry")) {

                imageView.setImageResource(R.drawable.blackberry_logo);

            } else {

                imageView.setImageResource(R.drawable.android_logo);
            }*/
            idOfProductHandle++;

        } else {

            gridView = (View) convertView;
        }
        //HANDLER NA PUSTKE W ACTIVITY
        if(idOfProductHandle>=gridValues.length){
            idOfProductHandle=0;
        }
        return gridView;
    }
}