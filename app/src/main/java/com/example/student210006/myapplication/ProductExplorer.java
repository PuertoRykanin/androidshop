package com.example.student210006.myapplication;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Button;
import android.app.Activity;
import android.view.View;
import android.view.View.OnClickListener;
import android.database.Cursor;
import android.database.sqlite.*;
import android.content.*;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;
import android.widget.GridView;

public class ProductExplorer extends AppCompatActivity {
    myDbAdapter DatabaseHandler; // Zmienna do zarządzania baza danych
    GridView gridView;
    static int[ ] GRID_DATA;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        Cursor cursor;
        SQLiteDatabase db;
        Context context;
        DatabaseHandler = new myDbAdapter(this);
        Log.d("what: ", "in onCreate start");
        super.onCreate(savedInstanceState);
        setContentView(R.layout.productexplorerview);
        //clickButton = (Button) findViewById(R.id.button2);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        // Sets the Toolbar to act as the ActionBar for this Activity window.
        // Make sure the toolbar exists in the activity and is not null

        toolbar.setTitle("Produkty");
        setSupportActionBar(toolbar);
        cursor = null;
        //db = DatabaseHandler.myhelper.getWritableDatabase();

        gridView = (GridView) findViewById(R.id.gridView1);

        // Set custom adapter (GridAdapter) to gridview

        GRID_DATA = new int[myDbAdapter.myhelper.getAllProductCount()];
        GRID_DATA = myDbAdapter.myhelper.getAllProductIds();
        gridView.setAdapter(  new GridViewAdapter( this, GRID_DATA ) );

        String empName = "";

//        cursor = db.rawQuery("SELECT Name FROM myTable WHERE Password = 'sss'",null);
        //if(cursor.getCount() > 0) {

    //    cursor.moveToFirst();
    //    empName = cursor.getString(cursor.getColumnIndex("Name"));
        //}


      //  cursor.close();

        //clickButton.setText(empName);
    }

    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_back, menu);

        return true;
    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.backArrow:
                Intent intent = new Intent(ProductExplorer.this, MainActivity.class);
                startActivity(intent);
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    public void goFavourites(View view){
        Intent intent = new Intent(ProductExplorer.this, FavouritesExplorer.class);
        startActivity(intent);
    }
    public void goBasket(View view){
        Intent intent = new Intent(ProductExplorer.this, BasketExplorer.class);
        startActivity(intent);
    }
    public void goSearch(View view){
        Intent intent = new Intent(ProductExplorer.this, SearchActivity.class);
        startActivity(intent);
    }
}
