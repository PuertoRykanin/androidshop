package com.example.student210006.myapplication;

import android.graphics.Color;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.content.Intent;
import android.widget.Spinner;
import java.util.ArrayList;
import java.util.List;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.TextView;
import android.database.sqlite.SQLiteDatabase;


public class MainActivity extends AppCompatActivity {
    //Button clickButton;
    myDbAdapter DatabaseHandler; // Zmienna do zarządzania baza danych
    Spinner spinner;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        //clickButton = (Button) findViewById(R.id.button);
        spinner = (Spinner) findViewById(R.id.spinner);
        DatabaseHandler = new myDbAdapter(this);
        //SQLiteDatabase db = DatabaseHandler.myhelper.getWritableDatabase();

        //myDbAdapter.myhelper.deleteDataBase();

        //myDbAdapter.myhelper.onCreate(db);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        // Sets the Toolbar to act as the ActionBar for this Activity window.
        // Make sure the toolbar exists in the activity and is not null
        toolbar.setTitle("Strona Główna");
        setSupportActionBar(toolbar);
    }

    public void goToProducts(View view){
        Intent intent = new Intent(MainActivity.this, ProductExplorer.class);
        startActivity(intent);
    }
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu, menu);

        MenuItem item = menu.findItem(R.id.spinner);
        Spinner spinner = (Spinner) item.getActionView();

        List<String> spinnerArray = new ArrayList<String>();


        spinnerArray.add("Nothing Selected");
        spinnerArray.add("Search");
        spinnerArray.add("Saved Search");
        spinnerArray.add("Favourites");
        spinnerArray.add("Basket");
        spinnerArray.add("Image Explorer");
        spinnerArray.add("Image Grid Explorer");
        spinnerArray.add("Exit/Logout");


        ArrayAdapter<String> adapter = new ArrayAdapter<String>(this,
                R.layout.spinnercustomlayout, spinnerArray);

        spinner.setAdapter(adapter);


        spinner.setOnItemSelectedListener(new OnItemSelectedListener() {

            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                ((TextView) parent.getChildAt(0)).setText(null);
                ((TextView) parent.getChildAt(0)).setTextColor(000000);
                Spinner mySpinner = (Spinner)findViewById(R.id.spinner);
                String txtFromSpinner = mySpinner.getSelectedItem().toString();
                if (txtFromSpinner.equals("Search")) {
                    Intent intent = new Intent(MainActivity.this, SearchActivity.class);
                    startActivity(intent);
                }
                if (txtFromSpinner.equals("Saved Search")) {
                    //
                }
                if (txtFromSpinner.equals("Exit/Logout")) {
                    //
                }
                if (txtFromSpinner.equals("Favourites")) {
                    Intent intent = new Intent(MainActivity.this, FavouritesExplorer.class);
                    startActivity(intent);
                }
                if (txtFromSpinner.equals("Basket")) {
                    Intent intent = new Intent(MainActivity.this, BasketExplorer.class);
                    startActivity(intent);
                }
                if (txtFromSpinner.equals("Image Explorer")) {
                    Intent intent = new Intent(MainActivity.this, ImageExplorer.class);
                    startActivity(intent);
                }
                if (txtFromSpinner.equals("Image Grid Explorer")) {
                    Intent intent = new Intent(MainActivity.this, ImageGridExplorer.class);
                    intent.putExtra("itemID", "1");
                    startActivity(intent);
                }
            }


            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {
                // TODO Auto-generated method stub

            }
        });

        return true;
    }
}
