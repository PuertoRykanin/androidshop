package com.example.student210006.myapplication;

import android.support.v4.view.MotionEventCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.GestureDetector;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.database.Cursor;
import android.content.*;
import android.database.sqlite.SQLiteDatabase;
import android.widget.GridView;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

public class DetailedExplorer extends AppCompatActivity {
    myDbAdapter DatabaseHandler; // Zmienna do zarządzania baza danych
    static int selected;
    int objCountGlob;
    TextView textCategory,textPrice,textName,textType,textPlayers;
    ImageView image;
    float startXValue = 1;

    @Override
    public boolean onTouchEvent(MotionEvent event) {
        float endXValue = 0;
        float x1 = event.getAxisValue(MotionEvent.AXIS_X);
        int action = MotionEventCompat.getActionMasked(event);
        switch (action) {
            case (MotionEvent.ACTION_DOWN):
                startXValue = event.getAxisValue(MotionEvent.AXIS_X);

                return true;

            case (MotionEvent.ACTION_UP):
                endXValue = event.getAxisValue(MotionEvent.AXIS_X);
                if (endXValue > startXValue) {
                    if (endXValue - startXValue > 100) {
                        List<ProductDTO> dtoList = new ArrayList<ProductDTO>();
                        if(selected<objCountGlob){
                            selected++;
                        }
                        else{
                            selected = 1;
                        }
                        dtoList = DatabaseHandler.myhelper.searchProducts(DatabaseHandler.myhelper.selectSearchProducts(selected,"-","-",0,"-","-","-"));
                        textName.setText("Nazwa: "+dtoList.get(0).getName());
                        textType.setText("Typ: "+dtoList.get(0).getType());
                        textCategory.setText("Kategoria Wiekowa: "+dtoList.get(0).getCategory());
                        textPrice.setText("Cena: "+String.valueOf(dtoList.get(0).getPrice())+" zł+");
                        textPlayers.setText("Ilość graczy: "+dtoList.get(0).getPlayers());
                        image.setImageResource(Integer.valueOf(dtoList.get(0).getImage()));

                    }
                }else {
                    if (startXValue -endXValue> 100) {
                        //Log.e("info", String.valueOf(selected));
                        List<ProductDTO> dtoList = new ArrayList<ProductDTO>();
                        if(selected>1){
                            selected--;
                        }
                        else{
                            selected = objCountGlob;
                        }
                        dtoList = DatabaseHandler.myhelper.searchProducts(DatabaseHandler.myhelper.selectSearchProducts(selected,"-","-",0,"-","-","-"));
                        textName.setText("Nazwa: "+dtoList.get(0).getName());
                        textType.setText("Typ: "+dtoList.get(0).getType());
                        textCategory.setText("Kategoria Wiekowa: "+dtoList.get(0).getCategory());
                        textPrice.setText("Cena: "+String.valueOf(dtoList.get(0).getPrice())+" zł");
                        textPlayers.setText("Ilość graczy: "+dtoList.get(0).getPlayers());
                        image.setImageResource(Integer.valueOf(dtoList.get(0).getImage()));


                    }
                }
                return true;


            default:
                return super.onTouchEvent(event);
        }

    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        Cursor cursor;
        SQLiteDatabase db;
        Context context;
        int itemId=1;

        DatabaseHandler = new myDbAdapter(this);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.detailedexplorerview);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        // Sets the Toolbar to act as the ActionBar for this Activity window.
        // Make sure the toolbar exists in the activity and is not null



        toolbar.setTitle("Szczegóły");
        setSupportActionBar(toolbar);

        objCountGlob = myDbAdapter.myhelper.getAllProductCount();

        ImageButton imageLeft, imageRight, imageFav, imageBasket, imageSearch;


        Bundle extras = getIntent().getExtras();
        if (extras != null) {
            itemId = extras.getInt("itemID");
            //Log.e("isput",itemId);
        }
        selected = itemId;

        textCategory = (TextView) findViewById(R.id.textViewCategory);
        textPrice = (TextView) findViewById(R.id.textViewPrice);
        textPlayers = (TextView) findViewById(R.id.textViewPlayers);
        textName = (TextView) findViewById(R.id.textViewName);
        textType = (TextView) findViewById(R.id.textViewType);
        image = (ImageView) findViewById(R.id.detailedImg);
        imageLeft = (ImageButton) findViewById(R.id.imageLeftSwitch2);
        imageRight = (ImageButton) findViewById(R.id.imageRightSwitch2);

        List<ProductDTO> dtoList = new ArrayList<ProductDTO>();
        dtoList = DatabaseHandler.myhelper.searchProducts(DatabaseHandler.myhelper.selectSearchProducts(selected,"-","-",0,"-","-","-"));

        textName.setText("Nazwa: "+dtoList.get(0).getName());
        textType.setText("Typ: "+dtoList.get(0).getType());
        textCategory.setText("Kategoria Wiekowa: "+dtoList.get(0).getCategory());
        textPrice.setText("Cena: "+String.valueOf(dtoList.get(0).getPrice())+" zł+");
        textPlayers.setText("Ilość graczy: "+dtoList.get(0).getPlayers());
        image.setImageResource(Integer.valueOf(dtoList.get(0).getImage()));

        imageLeft.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                //Log.e("info", String.valueOf(selected));
                List<ProductDTO> dtoList = new ArrayList<ProductDTO>();
                if(selected>1){
                    selected--;
                }
                else{
                    selected = objCountGlob;
                }
                dtoList = DatabaseHandler.myhelper.searchProducts(DatabaseHandler.myhelper.selectSearchProducts(selected,"-","-",0,"-","-","-"));
                textName.setText("Nazwa: "+dtoList.get(0).getName());
                textType.setText("Typ: "+dtoList.get(0).getType());
                textCategory.setText("Kategoria Wiekowa: "+dtoList.get(0).getCategory());
                textPrice.setText("Cena: "+String.valueOf(dtoList.get(0).getPrice())+" zł");
                textPlayers.setText("Ilość graczy: "+dtoList.get(0).getPlayers());
                image.setImageResource(Integer.valueOf(dtoList.get(0).getImage()));
            }
        });
        imageRight.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                List<ProductDTO> dtoList = new ArrayList<ProductDTO>();
                if(selected<objCountGlob){
                    selected++;
                }
                else{
                    selected = 1;
                }
                dtoList = DatabaseHandler.myhelper.searchProducts(DatabaseHandler.myhelper.selectSearchProducts(selected,"-","-",0,"-","-","-"));
                textName.setText("Nazwa: "+dtoList.get(0).getName());
                textType.setText("Typ: "+dtoList.get(0).getType());
                textCategory.setText("Kategoria Wiekowa: "+dtoList.get(0).getCategory());
                textPrice.setText("Cena: "+String.valueOf(dtoList.get(0).getPrice())+" zł+");
                textPlayers.setText("Ilość graczy: "+dtoList.get(0).getPlayers());
                image.setImageResource(Integer.valueOf(dtoList.get(0).getImage()));
            }
        });

        image.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(DetailedExplorer.this, ImageGridExplorer.class);
                intent.putExtra("itemID", String.valueOf(selected));
                startActivity(intent);
            }
        });


    }

    public void goFavourites(View view){
        Intent intent = new Intent(DetailedExplorer.this, FavouritesExplorer.class);
        startActivity(intent);
    }
    public void goBasket(View view){
        Intent intent = new Intent(DetailedExplorer.this, BasketExplorer.class);
        startActivity(intent);
    }
    public void goSearch(View view){
        Intent intent = new Intent(DetailedExplorer.this, SearchActivity.class);
        startActivity(intent);
    }
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_back, menu);

        return true;
    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.backArrow:
                Intent intent = new Intent(DetailedExplorer.this, MainActivity.class);
                startActivity(intent);
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }
}